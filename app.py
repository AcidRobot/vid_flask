from flask import Flask, render_template, Response
# from local_camera import Camera
import numpy as np
import cv2
import time
from os.path import abspath, dirname
# local modules
# from video import create_capture
from picamera.array import PiRGBArray
from picamera import PiCamera

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
camera.rotation = 180
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)


app = Flask(__name__)
app.root_path = abspath(dirname(__file__))


@app.route('/')
def index():
    return render_template('index.html')


# def gen(camera):
#     for frame in camera.capture_continuous(rawCapture,
#                                           format="bgr",
#                                           use_video_port=True):
#         # grab the raw NumPy array representing the image, then
#         # initialize the timestamp
#         # and occupied/unoccupied text
#         img = frame.array
#         yield (b'--frame\r\n'
#                b'Content-Type: image/jpeg\r\n\r\n' + img + b'\r\n')
#         t = clock()
#         dt = clock() - t
#         # clear the stream in preparation for the next frame
#         rawCapture.truncate(0)
#
#
# @app.route('/video_feed')
# def video_feed():
#     return Response(gen(),
#                     mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=3030)
